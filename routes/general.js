var express = require('express');
var router = express.Router();
var Promise = require('promise');
var https = require("https");
var config = require("../config.json");

var request = require("./request");
var auth = require('./authRequest.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var generalRequest = function(auth) {
		console.log(auth);

		var options = config[config.environment]["options"];

		var authObject = JSON.parse(auth);
		options.headers["Authorization"] = authObject.token_type 
			+ " " + authObject.access_token;
		options.headers["X-Unique-Transaction-ID"] = new Date().getTime();
		options.path = "/api/v1/terms/admin/general";
		options.method = "GET";

	    return request(options);
	}
	var data = null;
	auth().then(function(data) {
		generalRequest(data).then(function(data) {
			console.log('request')
			var jsonData = JSON.parse(data);

			var getUrl = function(pdfName) {
		      var parts = pdfName.split("/");
		      console.log(parts);
		      var fileName = parts[parts.length - 1];
		      console.log(fileName);
		      console.log(config[config.environment].pdfUrl + fileName)
		      return config[config.environment].pdfUrl + fileName;
			}
			console.log('before bucle')
			for (key in jsonData) {
				var item = jsonData[key];
				console.log(key, item)
				item.pdfEn = getUrl(item.pdfEn);
				item.pdfEs = getUrl(item.pdfEs);
				jsonData[key] = item;
			}
			console.log('result jsonData',  jsonData)
			res.send(jsonData);
			console.log('sent')
		}, function(err) {
			console.log('general err', err);
		})
	}, function(err) {
		console.log('error');
	})

});

module.exports = router;
