var config = require("../config.json");
var Promise = require("promise");
var https = require("https");
var request = require("./request");

module.exports = function() {

	var options = config[config.environment]["options"];
	
	options.path = "/auth/tsec/token?grant_type=client_credentials";
	options.headers["Authorization"] = config[config.environment]["auth"];
	
	options.method = "POST";

    return request(options);
}
