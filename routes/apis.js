var express = require('express');
var router = express.Router();
var Promise = require('promise');
var https = require("https");
var config = require("../config.json");

var request = require("./request");
var auth = require('./authRequest.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var apiRequest = function(auth) {
		console.log(auth);
		var authObject = JSON.parse(auth);

		var options = config[config.environment]["options"];
		options.headers["Authorization"] = authObject.token_type 
			+ " " + authObject.access_token;
		options.path = "/api/v1/terms/admin/apis";

		options.method = "GET";
		options.headers["X-Unique-Transaction-ID"] = new Date().getTime();

console.log(options)
	    return request(options);
	}
	var data = null;
	var environment = config.environment;
		auth().then(function(data) {
			apiRequest(data).then(function(data) {
				res.send(data);
			}, function(err) {
				console.log('general err', err);
			})

		}, function(err) {
			console.log('error');
		})

});

module.exports = router;
