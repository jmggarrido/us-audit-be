var Promise = require("promise");
var https = require("https");

module.exports = function(options) {
    return new Promise(function(resolve, reject) {

        console.log('request options', options);
    	var request = https.request(options, function(res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function() {
                resolve(output);
            });
        });

        request.on('error', function(err) {
            console.log(err);
            reject(err);
        });

        request.end();
    })
}
