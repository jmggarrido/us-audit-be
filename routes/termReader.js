var express = require('express');
var router = express.Router();
var Promise = require('promise');
var https = require("https");
var config = require("../config.json");

var request = require("./request");
var auth = require('./authRequest.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var termId = req.query.termId;

	var termRequest = function(auth, termId) {

		var options = config[config.environment]["options"];

		var authObject = JSON.parse(auth);

		var termObject = {
			    "host": options.host,
			    "port": options.port,
			    "method": "GET",
			    "path":  '/api/v1/terms/admin/apis/' + termId,
			    "headers": {
			        "content-type": "application/json",
			        "Authorization": authObject.token_type 
			+ " " + authObject.access_token,
					"X-Unique-Transaction-ID": new Date().getTime()
			    }
			}

	    return request(termObject);
	}

	var data = null;
	auth().then(function(data) {
		console.log('auth data', data);
		termRequest(data, termId).then(function(data) {
			res.send(data);
		}, function(err) {
			console.log('general err', err);
		})
	}, function(err) {
		console.log('error');
	})

});

module.exports = router;
